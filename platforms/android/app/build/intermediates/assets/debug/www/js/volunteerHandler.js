var volunteerHandler = {
  addVolunteer: function (activity, location, date, time, name) {
    databaseHandler.db.transaction(
      function (tx) {
        tx.executeSql(
          'INSERT INTO volunteer_tbl(activity, location, date, time, name) VALUES(?,?,?,?,?)',
          [activity, location, date, time, name],
          function (tx, results) {

          },
          function (tx, error) {
            console.log('Add Volunteer error: ' + error.message)
          }
        )
      },
      function (error) {
        console.log('Transaction error: ' + error.message)
      },
      function () {

      }
    )
  },
  loadVolunteers: function (displayVolunteers) {
    databaseHandler.db.readTransaction(
      function (tx) {
        tx.executeSql(
          'SELECT * FROM volunteer_tbl',
          [],
          function (tx, results) {
            displayVolunteers(results)
          },
          function (tx, error) {
            console.log('Loading Volunteer error: ' + error.message)
          }
        )
      }
    )
  },
  deleteVolunteer: function (id) {
    databaseHandler.db.transaction(
      function (tx) {
        tx.executeSql(
          'DELETE FROM volunteer_tbl WHERE id = ?',
          [id],
          function (tx, results) {

          },
          function (tx, error) {
            console.log('Deleting volunteer Error: ' + error.message)
          }
        )
      }
    )
  },
  updateVolunteer: function (id, newActivity, newName, newDate, newTime, newLocation, description) {
    databaseHandler.db.transaction(
      function (tx) {
        tx.executeSql(
          'UPDATE volunteer_tbl SET activity=?, name=?, date=?, time=?, location=?, description=? WHERE id=?',
          [newActivity, newName, newDate, newTime, newLocation, description, id],
          function (tx, results) {

          },
          function (tx, error) {
            console.log('Updating volunteer Error: ' + error.message)
          }
        )
      }
    )
  }
}