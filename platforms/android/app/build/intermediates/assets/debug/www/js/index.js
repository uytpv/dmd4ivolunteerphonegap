var app = {
  initialize: function () {
    this.bindEvents()
  },
  bindEvents: function () {
    document.addEventListener('deviceready', this.onDeviceReady, false)
  },
  onDeviceReady: function () {
    databaseHandler.createDatabase()
  },
}

function addVolunteer () {
  var activity = $('#txtActivity').val()
  var name = $('#txtName').val()
  var date = $('#date').val()
  var time = $('#time').val()
  var location = $('#location').val()

  if (validateVolunteer(activity, name, date, time)) {
    var r = confirm(
      'Please confirm the volunteer information? \n' +
      'Activity: ' + activity + '\n' +
      'Volunteer Name: ' + name + '\n' +
      'Date: ' + date + '\n' +
      'Time: ' + time + '\n' +
      'Location: ' + location
    )
    if (r === true) {
      volunteerHandler.addVolunteer(activity, location, date, time, name)
      $('form')[0].reset()
    }
  }
}

var currentVolunteer = {
  id: -1,
  name: '',
  activity: '',
}

function displayVolunteers (results) {
  var lenght = results.rows.length
  var lstVoulunteers = $('#lstVolunteers')
  lstVoulunteers.empty() // Clean the old data
  for (var i = 0; i < lenght; i++) {
    var item = results.rows.item(i)
    var li = $('<li />')
    var a = $('<a />')
    var h3 = $('<h3 />').text('Activity: ')
    var h4 = $('<h4 />').text('by  ')
    var p = $('<p />').text('from ')
    var p2 = $('<p />').text('Description: ')
    var small = $('<small />').text('')
    var hidden = $('<input/>').val(item.id)
    hidden.attr('name', 'id')
    hidden.attr('type', 'hidden')
    var spanActivity = $('<span />').text(item.activity)
    spanActivity.attr('name', 'activity')
    var spanName = $('<span />').text(item.name)
    spanName.attr('name', 'name')
    var spanLocation = $('<span />').text(item.location)
    spanLocation.attr('name', 'location')

    var spanDesc = $('<span />').text(item.description)
    spanDesc.attr('name', 'description')

    var spanDate = $('<span />').text(item.date)
    spanDate.attr('name', 'date')
    var spanTime = $('<span />').text(item.time)
    spanTime.attr('name', 'time')
    h3.append(spanActivity)
    h4.append(spanName)
    p.append(spanLocation)
    p2.append(spanDesc)
    small.append(spanDate)
    small.append(' - ')
    small.append(spanTime)
    a.append(h3)
    a.append(h4)
    a.append(p)
    a.append(p2)
    a.append(small)
    a.append(hidden)

    li.append(a)

    lstVoulunteers.append(li)
  }

  lstVoulunteers.listview('refresh')
  lstVoulunteers.on('tap', 'li', function () {
    currentVolunteer.id = $(this).find('[name="id"]').val()
    currentVolunteer.activity = $(this).find('[name="activity"]').text()
    currentVolunteer.name = $(this).find('[name="name"]').text()
    currentVolunteer.date = $(this).find('[name="date"]').text()
    currentVolunteer.time = $(this).find('[name="time"]').text()
    currentVolunteer.location = $(this).find('[name="location"]').text()
    currentVolunteer.description = $(this).find('[name="description"]').text()
    $('#popupUpdateDelete').popup('open')
  })
}

$(document).on('pagebeforeshow', '#listpage', function () {
  volunteerHandler.loadVolunteers(displayVolunteers)
})

$(document).on('pagebeforeshow', '#editpage', function () {
  $('#txtNewActivity').val(currentVolunteer.activity)
  $('#txtNewName').val(currentVolunteer.name)
  $('#newDate').val(currentVolunteer.date)
  $('#newTime').val(currentVolunteer.time)
  $('#newLocation').val(currentVolunteer.location).change()
  $('#newDesc').val(currentVolunteer.description)
})

function deleteVolunteer () {
  var r = confirm('Do you want to delete\nActivity: ' + currentVolunteer.activity + '\n By: ' + currentVolunteer.name)
  if (r === true) {
    volunteerHandler.deleteVolunteer(currentVolunteer.id)
    volunteerHandler.loadVolunteers(displayVolunteers)
  }
  $('#popupUpdateDelete').popup('close')
}

function validateVolunteer (activity, name, date, time) {
  var activityValidate = false
  var nameValidate = false
  var dateValidate = false
  var timeValidate = false

  if (!activity) {
    alert('Activity is required')
    return
  } else {
    if (activity.length < 3 || activity.length > 20) {
      alert('Activity should be at least has 3 and not more than 20 characters in length')
      return
    }
    activityValidate = true
  }
  if (!name) {
    alert('Volunteer name is required')
    return
  } else {
    if (activity.length < 3 || activity.length > 20) {
      alert('Name should be at least has 3 and not more than 20 characters in length')
      return
    }
    nameValidate = true
  }
  if (!date) {
    alert('Date is required')
    return
  } else {
    var curDate = getCurrentDate()
    var chosenDate = parseInt(date.replace(/-/g, ''))
    if (chosenDate <= curDate) {
      alert('Chosen date must be in the future')
      return
    }
    dateValidate = true
  }
  if (!time) {
    alert('Time is required')
    return
  } else {
    timeValidate = true
  }

  return activityValidate && nameValidate && dateValidate && timeValidate
}

function updateVounteer () {
  var activity = $('#txtNewActivity').val()
  var name = $('#txtNewName').val()
  var date = $('#newDate').val()
  var time = $('#newTime').val()
  var location = $('#newLocation').val()
  var description = $('#newDesc').val()

  if (validateVolunteer(activity, name, date, time)) {
    var r = confirm(
      'Please confirm the volunteer information? \n' +
      'Activity: ' + activity + '\n' +
      'Volunteer Name: ' + name + '\n' +
      'Date: ' + date + '\n' +
      'Time: ' + time + '\n' +
      'Location: ' + location
    )
    if (r === true) {
      volunteerHandler.updateVolunteer(currentVolunteer.id, activity, name, date, time, location, description)
      window.location.href = '#listpage'
    }
  }
}

function getCurrentDate () {
  var d = new Date()

  var month = d.getMonth() + 1
  var day = d.getDate()

  var output = d.getFullYear() +
    (month < 10 ? '0' : '') + month +
    (day < 10 ? '0' : '') + day
  return parseInt(output)
}