var databaseHandler = {
  db: null,
  createDatabase: function () {
    this.db = window.sqlitePlugin.openDatabase({
      name: 'volunteerDb.db',
      location: 'default',
    })
    // this.db = window.openDatabase(
    //     "volunteerDb",
    //     "1.0",
    //     "Volunteer Database",
    //     3 * 1024 * 1024
    // );
    this.db.transaction(
      function (tx) {
        // Run executing sql here using tx
        tx.executeSql(
          'CREATE TABLE IF NOT EXISTS volunteer_tbl (id INTEGER PRIMARY KEY, activity TEXT NOT NULL, location TEXT, date TEXT NOT NULL, time TEXT, name TEXT NOT NULL, description TEXT)'),
          [],
          function (tx, results) {
          },
          function (tx, error) {
            console.log('Error while create the table: ' + error.message)
          }
      },
      function (error) {
        console.log('Transaction error' + error.message)
      },
      function () {
        console.log('Create DB transaction complete successfully')
      }
    )

  }
}